import sublime, sublime_plugin
import re

class OdcFixJavaCommand(sublime_plugin.TextCommand):

    def run(self, edit):
        self.memcmp()
        self.memcpy()
        self.fix()
        self.sizeof()
        self.need_fix()
        self.format()

        self.output()

    def is_enabled(self):
        return not self.view.is_read_only()

    def is_visible(self):
        return True

    def description():
        return None

    # --------------------------------------------

    def memcpy(self): 
        view  = self.view
        alltextreg = sublime.Region(0, view.size())
        text = view.substr(alltextreg)
        # begin replace
        edit = view.begin_edit()

        # fix: memcpy(obj1, obj2, len);
        text = re.compile(r'memcpy\(\s*(.*)\,\s*(.*),\s*(.*)\);').sub(r'\1 = \2; /* LEN: \3 */', text)
        # fix: a = memcpy(obj1, obj2);
        text = re.compile(r'=\s*memcpy\(\s*(.*)\,\s*(.*)\);').sub(r'= \2;', text)
        # fix: memcpy(obj)
        text = re.compile(r'=\s*memcpy\(\s*(.*)\);').sub(r'= \1.toString();// FIXME obj.toString();', text)
        
        view.end_edit(edit)
        # end replace
        view.replace(edit, alltextreg, text)
        
    def memcmp(self): 
        view  = self.view
        alltextreg = sublime.Region(0, view.size())
        text = view.substr(alltextreg)
        # begin replace
        edit = view.begin_edit()
        
        # fix: memcmp(s1, s2, len) == 0
        # fix: memcmp(s2, s2, len) != 0
        text = re.compile(r'memcmp\((.*),\s*(.*),\s*(.*)\)[\s]*(!?)(=?)=.*0')\
        .sub(r'\4\2.equals(\1) /* LEN: \3 */', text)
        
        view.end_edit(edit)
        # end replace
        view.replace(edit, alltextreg, text)

    def sizeof(self):
        view  = self.view
        alltextreg = sublime.Region(0, view.size())
        text = view.substr(alltextreg)
        # begin replace
        edit = view.begin_edit()

        # add: public final static int SIZEOF_WD_ATAGICBATL = N;
        text = re.compile(r'(@Parameter.*length\s*=\s*)([\d|\w|_|\.]*)(.*(\s*)(\w*)\s*(\w*)\s*(\w*);)')\
        .sub(r'public static final int SIZEOF_\7 = \2;\4\1 SIZEOF_\7\3', text)

        # local var
        text = re.compile(r'sizeof\(([A-Z0-9_]*)\)').sub(r'SIZEOF_\1', text)
        # sizeof class
        text = re.compile(r'sizeof\((.*).class\)').sub(r'\1.sizeOf()', text)
        # sizeof obj.oc
        text = re.compile(r'sizeof\((.*).class,\s*"oc"\)').sub(r'\1.SIZEOF_OC * XXXX.sizeOf()', text)
        # sizeof obj.xxx
        text = re.compile(r'sizeof\((.*).class,\s*"(\w*)"\)').sub(r'\1.SIZEOF_\2', text)
        # sizeof(obj)
        text = re.compile(r'sizeof\(([\w]+)\)').sub(r'\1.sizeOf()', text)
        # sizeof(a.b.attr)
        text = re.compile(r'sizeof\(([\w\.\[\]]+)\.(\w+)\)').sub(r'\1.SIZEOF_\2', text)

        view.end_edit(edit)
        # end replace
        view.replace(edit, alltextreg, text)

    def fix(self):
        view  = self.view
        alltextreg = sublime.Region(0, view.size())
        text = view.substr(alltextreg)
        # begin replace
        edit = view.begin_edit()

        # fix: &aaa = &bbb;
        text = re.compile(r'&?(.?)\s?=\s?&?(.*);').sub(r'\1 = \2;', text)
        text = re.compile(r'([\+\-\*\/]) \=').sub(r'\1=', text)
        # fix: error.leng = xx.text.length();
        text = re.compile(r'(.*\.leng)\s*=\s*([\w\_]*\.text\.length\(\));')\
        .sub(r'\1 = new Integer(\2).shortValue();', text)
        # del: empty print
        text = re.compile(r'(\s*)System\.out\.print\(\);(\s*)').sub(r'\1\2', text)
        # del: empty print
        text = re.compile(r'System\.out\.print\(String\.format\(\)\);\s*').sub(r'',text)
        # fix: sprintf
        text = re.compile(r'sprintf\s*\(([\w\._ ]*),(.*)\);').sub(r'\1 = String.format(\2);', text)
        # fix: printf
        text = re.compile(r'([\{?|\)?|\s?])(printf\(.*\));*').sub(r'\1System.out.\2;', text)
        # fix: strcpy
        text = re.compile(r'strcpy\((.*),(.*)\);').sub(r'\1 = \2;', text)
        # fix: tk_comm.Tk_Error
        text = re.compile(r'tk_comm.TK_Error').sub(r'TK_Error', text)
        # fix: RBS_DEFINE.DB_OK
        text = re.compile(r'if\s*\((it_txcom\.rtncd = sqlcode = [\(\)\,\w\s_\.]*\)) != RBS_DEFINE.DB_OK\)')\
        .sub(r'if ((\1) != RBS_DEFINE.DB_OK)',text)

        view.end_edit(edit)
        # end replace
        view.replace(edit, alltextreg, text)

    def need_fix(self):
        view  = self.view
        alltextreg = sublime.Region(0, view.size())
        text = view.substr(alltextreg)
        # begin replace
        edit = view.begin_edit()

        # fix: it_totw.labtex.label.msglng = String.format(
        text = re.compile(r'(msglng = String\.format.*)(\s)').sub(r'\1 // FIXME MSGLNG = String.format() \2', text)

        view.end_edit(edit)
        # end replace
        view.replace(edit, alltextreg, text)

    def format(self):
        view  = self.view
        alltextreg = sublime.Region(0, view.size())
        text = view.substr(alltextreg)
        # begin replace
        edit = view.begin_edit()

        # fix: somecode____{
        text = re.compile(r'\s*\{').sub(r' {',text)
        # fix: } else if {
        text = re.compile(r'\}\s*else\s*\{').sub(r'} else {',text)
        # fix: { xxxxxxx;
        text = re.compile(r'\{[ \t]*(\w)').sub(r'{\n\1',text)
        # fix: xxxxxxx; }
        text = re.compile(r';[ \t]*\}').sub(r';\n}',text)

        view.end_edit(edit)
        # end replace
        view.replace(edit, alltextreg, text)

    def output(self):
        sublime.status_message('Process complete successfully :)')