# ODCTools

ODCTools is a simple plugin for [Sublime Text2][st2], it is used to fix simple mistake in java code, which is converted from ProC by ProC Conversion Tool 2012.

## Features

1. fix memcpy
2. fix memcmp
3. fix strcpy
4. fix error.leng
5. fix print/printf/sprintf
6. fix Tk_Error
7. add static variables(sizeof)
8. code format
9. and many more...

## Usage

1. [Download][download] plugin.
2. Unzip plugin and rename to "_ODC-TOOLS_".
3. Open [Sublime Text2][st2], Click 'Preferences' - > 'Browse Packages...' to open package directory.
4. Copy "_ODC-Tools_" to package directory.
5. Restart [Sublime Text2][st2].
6. Right click on the file editor, select 'ODCTools - FixJava'.

## Reference Doc

+ [Sublime Text 2 Documentation][doc]
+ [API Reference][api]
+ [How to Create a Sublime Text 2 Plugin][cre-plugin]
	
## Contact

+ 张露兵 (Colin Zhang)
+ Email: zhanglubing927[at]163.com
+ Blog: [http://zhanglubing.github.com][blog]

[st2]: http://www.sublimetext.com/
[doc]: http://www.sublimetext.com/docs/2/
[api]: http://www.sublimetext.com/docs/2/api_reference.html
[cre-plugin]: http://net.tutsplus.com/tutorials/python-tutorials/how-to-create-a-sublime-text-2-plugin/
[download]: https://bitbucket.org/colinz/odc-tools/get/master.zip
[blog]: http://zhanglubing.github.com
